<table>
<thead>
<tr><th>这个作业属于哪个课程</th><th><a href="https://edu.cnblogs.com/campus/zswxy/computer-science-class1-2018" target="_blank">软工-2018级计算机1班</a></th></tr>
</thead>
<tbody>
<tr>
<td>这个作业要求在哪里</td>
<td>&nbsp; &nbsp;&nbsp;<a href="https://edu.cnblogs.com/campus/zswxy/computer-science-class2-2018/homework/11878" target="_blank">202103226-1 编程作业</a></td>
</tr>
<tr>
<td>这个作业的目标</td>
<td>实践中学习软件工程</td>
</tr>
<tr>
<td>学号</td>
<td>20188385</td>
</tr>
</tbody>
</table>
<h1>Gitee仓库地址</h1>
<p>&nbsp;</p>
<p>&nbsp;<a href="https://gitee.com/harsonyoung_admin/project-java" target="_blank">Project-Java-Gitee</a></p>
<p>&nbsp;</p>
<h1>解题思路描述</h1>
<p>&nbsp; &nbsp;看完题目描述，第一时间就想到了HashMap，单词-数量;这不就一键值对嘛。一把嗦秒杀！这会不会太轻松了，于是自己开始整(zuo)活(si)就想着能不能不用HashMap解决，用纯字节流完成，性能会不会更好。用KMP搞了半天，结果大翻车，彻底暴露了我这个算法菜鸡的本质。最后还是老老实实的用HashMap了。</p>
<p>&nbsp; 决定用HashMapd话思路就很清晰了，先把文本一个个字节进行一定的处理和判断后一个个单词放进HashMap里面，键为单词字符串，值为频次。全部放进去之后，问题来了，频次高者优先输出很好办，在频次相同时如何按照单词字典序输出呢？一番搜索后，学习到了TreeMap这个类，它的底层是红黑树，默认插入进去的Key为Sting类时，就会按照字典序排序。太好了！这下所有问题都解决了，啪的一下，做一些输出处理，完成了。</p>
<p>&nbsp;</p>
<h1>代码规范链接</h1>
<p>&nbsp;</p>
<p><a href="https://gitee.com/harsonyoung_admin/project-java/blob/master/20188385/src/codestyle.md" target="_blank">codestyle.md-Gitee</a></p>
<p>&nbsp;</p>
<h1>计算模块接口的设计与实现过程</h1>
<p>&nbsp;<img src="https://img2020.cnblogs.com/blog/1581412/202103/1581412-20210330002207189-1608783126.png" alt="" loading="lazy" /></p>
<p>&nbsp;</p>
<p><strong>&nbsp;fileProcess()</strong></p>
<p><strong><br /></strong>该方法实现读取文件，判断是否为纯ASCII编码文件，并做了非字母数字过滤，最后返回的是纯数字字母的字符串数组。</p>
<div class="cnblogs_code">
<pre><span style="color: #008080;"> 1</span>     <span style="color: #0000ff;">public</span> <span style="color: #0000ff;">static</span> String[] fileProcess(String filePath) <span style="color: #0000ff;">throws</span><span style="color: #000000;"> IOException {
</span><span style="color: #008080;"> 2</span>         StringBuilder buffer = <span style="color: #0000ff;">new</span><span style="color: #000000;"> StringBuilder();
</span><span style="color: #008080;"> 3</span>         InputStream file = <span style="color: #0000ff;">new</span><span style="color: #000000;"> FileInputStream(filePath);
</span><span style="color: #008080;"> 4</span>         <span style="color: #0000ff;">int</span> size =<span style="color: #000000;"> file.available();
</span><span style="color: #008080;"> 5</span> 
<span style="color: #008080;"> 6</span>         <span style="color: #0000ff;">for</span> (<span style="color: #0000ff;">int</span> i = 0; i&lt;size;i++<span style="color: #000000;">){
</span><span style="color: #008080;"> 7</span>             <span style="color: #0000ff;">char</span> byt = (<span style="color: #0000ff;">char</span><span style="color: #000000;">)file.read();
</span><span style="color: #008080;"> 8</span>             <span style="color: #008000;">//</span><span style="color: #008000;">Check illegal character</span>
<span style="color: #008080;"> 9</span>             <span style="color: #0000ff;">if</span> (byt &gt; 127<span style="color: #000000;">){
</span><span style="color: #008080;">10</span>                 System.out.println("[-]Input pure ASCII file only!"<span style="color: #000000;">);
</span><span style="color: #008080;">11</span>                 System.exit(1<span style="color: #000000;">);
</span><span style="color: #008080;">12</span> <span style="color: #000000;">            }
</span><span style="color: #008080;">13</span>             <span style="color: #008000;">//</span><span style="color: #008000;">Convenient for split</span>
<span style="color: #008080;">14</span>             <span style="color: #0000ff;">if</span>(byt == '\n'<span style="color: #000000;">){
</span><span style="color: #008080;">15</span>                 byt = ' '<span style="color: #000000;">;
</span><span style="color: #008080;">16</span> <span style="color: #000000;">            }
</span><span style="color: #008080;">17</span>             <span style="color: #008000;">//</span><span style="color: #008000;">Filter exclude number and letter</span>
<span style="color: #008080;">18</span>             <span style="color: #0000ff;">else</span> <span style="color: #0000ff;">if</span>(byt == '\r'      ||
<span style="color: #008080;">19</span>                     byt&gt;='!' &amp;&amp; byt&lt;='/' ||
<span style="color: #008080;">20</span>                     byt&gt;=':' &amp;&amp; byt&lt;='@' ||
<span style="color: #008080;">21</span>                     byt&gt;='[' &amp;&amp; byt&lt;='`' ||
<span style="color: #008080;">22</span>                     byt&gt;='{'
<span style="color: #008080;">23</span> <span style="color: #000000;">            ){
</span><span style="color: #008080;">24</span>                 <span style="color: #0000ff;">continue</span><span style="color: #000000;">;
</span><span style="color: #008080;">25</span> <span style="color: #000000;">            }
</span><span style="color: #008080;">26</span> <span style="color: #000000;">            buffer.append(byt);
</span><span style="color: #008080;">27</span> <span style="color: #000000;">        }
</span><span style="color: #008080;">28</span> 
<span style="color: #008080;">29</span>         String text =<span style="color: #000000;"> buffer.toString().toLowerCase();
</span><span style="color: #008080;">30</span>         String[] division = text.split(" "<span style="color: #000000;">);
</span><span style="color: #008080;">31</span>         <span style="color: #0000ff;">return</span><span style="color: #000000;"> division;
</span><span style="color: #008080;">32</span>     }</pre>
</div>
<p>&nbsp;</p>
<p><strong>wordCounter()</strong></p>
<p>词频计数的核心方法，将字符串一个个填到TreeMap里，内置的红黑树将Key排好序，直接按Value从大到小拿出十个就好了，拿出来的Key-Value放在有序的LinkedHashMap内。</p>
<div class="cnblogs_code">
<pre><span style="color: #008080;"> 1</span>     <span style="color: #0000ff;">public</span> <span style="color: #0000ff;">static</span> LinkedHashMap&lt;String,Integer&gt;<span style="color: #000000;"> wordCounter(String[] division){
</span><span style="color: #008080;"> 2</span>         TreeMap&lt;String,Integer&gt; counter = <span style="color: #0000ff;">new</span> TreeMap&lt;&gt;<span style="color: #000000;">();
</span><span style="color: #008080;"> 3</span>         LinkedHashMap&lt;String,Integer&gt; result = <span style="color: #0000ff;">new</span> LinkedHashMap&lt;&gt;<span style="color: #000000;">();
</span><span style="color: #008080;"> 4</span> 
<span style="color: #008080;"> 5</span>         <span style="color: #0000ff;">for</span><span style="color: #000000;"> (String s : division) {
</span><span style="color: #008080;"> 6</span>             <span style="color: #0000ff;">if</span><span style="color: #000000;">(stringChecker(s)){
</span><span style="color: #008080;"> 7</span>                 <span style="color: #0000ff;">continue</span><span style="color: #000000;">;
</span><span style="color: #008080;"> 8</span> <span style="color: #000000;">            }
</span><span style="color: #008080;"> 9</span>             <span style="color: #0000ff;">if</span><span style="color: #000000;"> (counter.containsKey(s)) {
</span><span style="color: #008080;">10</span>                 Integer count =<span style="color: #000000;"> counter.get(s);
</span><span style="color: #008080;">11</span>                 counter.put(s, count + 1<span style="color: #000000;">);
</span><span style="color: #008080;">12</span>             } <span style="color: #0000ff;">else</span><span style="color: #000000;"> {
</span><span style="color: #008080;">13</span>                 counter.put(s, 1<span style="color: #000000;">);
</span><span style="color: #008080;">14</span> <span style="color: #000000;">            }
</span><span style="color: #008080;">15</span> <span style="color: #000000;">        }
</span><span style="color: #008080;">16</span> 
<span style="color: #008080;">17</span>         <span style="color: #0000ff;">for</span> (<span style="color: #0000ff;">int</span> i = 0; i&lt;10; i++<span style="color: #000000;">){
</span><span style="color: #008080;">18</span>             String maxWord = ""<span style="color: #000000;">;
</span><span style="color: #008080;">19</span>             Integer maxNum = 0<span style="color: #000000;">;
</span><span style="color: #008080;">20</span>             <span style="color: #0000ff;">for</span><span style="color: #000000;"> (String word : counter.keySet()){
</span><span style="color: #008080;">21</span>                 Integer currentNum =<span style="color: #000000;"> counter.get(word);
</span><span style="color: #008080;">22</span>                 <span style="color: #0000ff;">if</span>(currentNum &gt;<span style="color: #000000;"> maxNum){
</span><span style="color: #008080;">23</span>                     maxNum =<span style="color: #000000;"> currentNum;
</span><span style="color: #008080;">24</span>                     maxWord =<span style="color: #000000;"> word;
</span><span style="color: #008080;">25</span> <span style="color: #000000;">                }
</span><span style="color: #008080;">26</span> <span style="color: #000000;">            }
</span><span style="color: #008080;">27</span> <span style="color: #000000;">            counter.remove(maxWord);
</span><span style="color: #008080;">28</span> <span style="color: #000000;">            result.put(maxWord, maxNum);
</span><span style="color: #008080;">29</span> <span style="color: #000000;">        }
</span><span style="color: #008080;">30</span>         <span style="color: #0000ff;">return</span><span style="color: #000000;"> result;
</span><span style="color: #008080;">31</span>     }</pre>
</div>
<p>&nbsp;</p>
<p><strong>resultOutput()</strong></p>
<p>输出结果到文件，由于LinkedHashMap是有序的，只要遍历拿出数据写入文件就行了。</p>
<p>&nbsp;</p>
<p><strong>countWord()</strong></p>
<p>此方法和wordCounter方法很类似，只要把符合要求的字符串计数返回就完成了。</p>
<p>&nbsp;</p>
<p><strong>countChar()</strong></p>
<p>这个就更简单了，读取文件后，file类就有字节数属性，直接返回。</p>
<h1>计算模块接口部分的性能测试和改进</h1>
<p>&nbsp;测试文本为Game Of Thrones部分小说。约三百万字节</p>
<p><img src="https://img2020.cnblogs.com/blog/1581412/202103/1581412-20210330003828934-909981694.png" alt="" width="355" height="537" loading="lazy" /></p>
<p>&nbsp;</p>
<p>&nbsp;使用JProFiler进行性能测试</p>
<p>&nbsp;<img src="https://img2020.cnblogs.com/blog/1581412/202103/1581412-20210330003929467-2028775849.png" alt="" loading="lazy" /></p>
<p>&nbsp;</p>
<p>&nbsp;我的笔记本是四核CPU，程序运行时占用约25%的CPU，可以认为占满了一整个CPU内核，程序运行了约8秒后结束。</p>
<p>&nbsp;<img src="https://img2020.cnblogs.com/blog/1581412/202103/1581412-20210330004314962-1610980282.png" alt="" loading="lazy" /></p>
<p>&nbsp;</p>
<p>&nbsp;观察处理器占用后发现，fileProcess占用了超过90%的时间，猜测是fileProcess方法使用了太多if语句，因此CPU的乱序执行和分支预测效率都很低下，运行速度因此拖慢。</p>
<p>但我没有想到更好的办法来减少语句执行的数量，除非放弃输入检查，那么也放弃了单词统计的严格性。</p>
<p><img src="https://img2020.cnblogs.com/blog/1581412/202103/1581412-20210330005035862-1799020930.png" alt="" loading="lazy" /></p>
<p>&nbsp;</p>
<p>&nbsp;内存占用方面，String和byte大小类似是因为读取文件时采用了StringBuffer，这里是方便做后续处理，在输入完毕后的阶段JVM也对byte[]进行了GC操作，总内存占用不到输入文件的两倍，总体来说上内存占用还是比较理想的。</p>
<p>最大的优化的方向就是在输入的过程中，将byte[]直接处理转换为String[]但时间效率是否受影响就不得而知了。</p>
<h1>计算模块部分单元测试展示</h1>
<p>&nbsp;<img src="https://img2020.cnblogs.com/blog/1581412/202103/1581412-20210330005907030-648917553.png" alt="" loading="lazy" /></p>
<p>&nbsp;</p>
<p>由于不知道如何实现脚本单元测试覆盖率，故按照要求标准手动测试。</p>
<p><span style="text-decoration: line-through;">lord居然在权游小说词频前10，怪不得我看完电视剧版就学会了一句Yes,my lord!</span></p>
<h1>计算模块部分异常处理说明</h1>
<p>&nbsp;不输入参数运行，正常提示消息。</p>
<p><img src="https://img2020.cnblogs.com/blog/1581412/202103/1581412-20210330010632722-1311933231.png" alt="" loading="lazy" /></p>
<p>&nbsp;</p>
<p>尝试导入一个压缩包，正常报错退出。</p>
<p><img src="https://img2020.cnblogs.com/blog/1581412/202103/1581412-20210330010348902-836254515.png" alt="" loading="lazy" /></p>
<p>&nbsp;</p>
<p>&nbsp;假装输错文件名，正常捕获异常。</p>
<p><img src="https://img2020.cnblogs.com/blog/1581412/202103/1581412-20210330010450783-1570515823.png" alt="" loading="lazy" /></p>
<h1>PSP表格</h1>
<p>&nbsp;<img src="https://img2020.cnblogs.com/blog/1581412/202103/1581412-20210330010731666-1123729292.png" alt="" loading="lazy" /></p>
<p>&nbsp;&nbsp;　注：加号代表在实际操作时未准确计时，只是一个保守的估计。</p>
<h1>心路历程与收获</h1>
<p>　　这个作业最大的感受就是有点杀鸡用牛刀的感觉。我非常能理解PSP表格中各个项目对应软件开发的各个流程。但我认为PSP更适合做需要长期维护的项目，而不是像这次作业的算法练习题。例如算法题不需要非常详细的设计文档，往往三言两语就能将核心思想交代清楚、算法题不需要反复的review，因为算法题往往目标是更高效的计算某个东西而不是更好的代码可读性（当然这也是非常重要的，但许多算法高手写出来的代码通常可读性较差）和更好的可维护性等等。</p>
<p>　　在这次实践中也学习到了Java中Map家族类的一些属性和方法，并且发现使用Java内置包的方法真的速度很快，我最初会以为核心方法wordCounter耗时应该是最长的，结果测试出来后反而是我写的过滤判断慢，使用红黑树的TreeMap排序真的很快。这次的作业总体来说难度不算太大（比数独简单），但也充分地体现了在软件工程中实践的一些流程。</p>