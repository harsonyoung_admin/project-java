import java.io.*;

public class WordCount {
    public static void main(String[] args)  {
        if(args.length != 2){
            System.out.println("[*]Two parameter is necessary!");
            System.out.println("[*]Usage: java WordCount input.txt output.txt");
            System.exit(1);
        }
        String inputPath = args[0];
        String outputPath = args[1];
        try {
            WordCountCore.countWordFrequency(inputPath, outputPath);
        }catch (IOException e){
            System.out.println("[-]IO Exception: " + e);
        }
    }

}
